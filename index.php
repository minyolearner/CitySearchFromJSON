<!DOCTYPE html>
<html>
<head>
    <title>City Search</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="script.js"></script>
</head>
<body>
    <h1>City Search</h1>
    <input type="text" id="search-input" placeholder="Search for a city...">
    
    <div id="search-results"></div>
</body>
</html>