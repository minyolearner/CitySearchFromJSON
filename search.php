<?php
// Load the JSON database file into an array
$database = json_decode(file_get_contents("cities.json"), true);

// Check if a search term was provided
if (isset($_POST["search"])) {
    // Sanitize the search term
    $searchTerm = htmlspecialchars($_POST["search"]);

    // Search the database for cities that match the search term
    $results = array();
    foreach ($database as $city) {
        if (stripos($city["name"], $searchTerm) !== false) {
            $results[] = $city;
        }
    }

    // Generate the HTML for the search results
    $html = "";
    if (count($results) > 0) {
        foreach ($results as $city) {
            $html .= "<div>" . $city["name"] . ", " . $city["country"] . "</div>";
        }
    } else {
        $html .= "<div>No results found</div>";
    }

    // Return the HTML to the client
    echo $html;
}
?>