$(document).ready(function() {
    // Add an event listener to the search input field
    $("#search-input").on("input", function() {
        // Get the value of the search input field
        var searchValue = $(this).val();

        // Send an AJAX request to the server to search for cities
        $.ajax({
            type: "POST",
            url: "search.php",
            data: { search: searchValue },
            dataType: "html",
            success: function(response) {
                // Update the search results section with the HTML returned by the server
                $("#search-results").html(response);
            },
            error: function(xhr, status, error) {
                // Handle errors
                console.error(error);
            }
        });
    });
});