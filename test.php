<?php
// Set up the booking request with the token in the Authorization header
$booking_request = curl_init($booking_url);
curl_setopt($booking_request, CURLOPT_POST, true);
curl_setopt($booking_request, CURLOPT_POSTFIELDS, $booking_data_json);
curl_setopt($booking_request, CURLOPT_RETURNTRANSFER, true);
curl_setopt($booking_request, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json",
    "Authorization: Bearer $token"
));

// Make the booking request
$booking_response = curl_exec($booking_request);

// encode to array

$booking_response_json = json_decode($booking_response,true);
echo $booking_response_json['bookingid'];


echo "</br>";


// Print the booking response
echo $booking_response;
